import { createStore } from 'vuex'

import actions from "@/vuex/actions/actions";
import mutations from "@/vuex/mutations/mutations";
import getters from "@/vuex/getters/getters";

export const store = createStore({
    state: {
        searchValue: '',
        isMobile: false,
        isDesktop: true,
        products: [],
        cart: [],
    },
    mutations,
    actions,
    getters
});

